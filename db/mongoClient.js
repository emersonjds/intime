const Mongoose = require('mongoose');

let connected = false

function connectDb(){

  return new Promise((resolve, reject) => {
    
    if(connected){
      resolve()
      return
    }
  
    Mongoose.connect(process.env.MONGO_URL, { useNewUrlParser: true })

    const connection = Mongoose.connection
    connection.once('open', () => {
      console.log('db rodando')
      connected = true
      resolve()
    })

  })
}

module.exports = connectDb



