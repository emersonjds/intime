const Schema = require('mongoose').Schema;

const revenueSchema = new Schema({
    _id: Schema.Types.ObjectId,
    product: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        require: true,
    },
    description: {
        type: String,
        required: true
    },
    amount: {
        type: Number,
        required: true
    },
    erp_product_code: {
        type: Number,
        required: true
    }
})

module.exports = revenueSchema;