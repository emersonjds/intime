const Schema = require('mongoose').Schema;

const EnterpriseSchema = new Schema({
  social_name: {
    type: String,
    required: true
  },
  fantasy_name: {
    type: String,
    required: true
  },
  cnpj: String,
  ie: Number,
  avatar: String
});

module.exports = EnterpriseSchema;