const Schema = require('mongoose').Schema;
const bankSchema = require('./bankSchema');
const EnterpriseSchema = require('./enterpriseSchema');

const ColaboratorsSchema = new Schema({
  erp_code: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  gender: {
    type: Number,
    required: true
  },
  date_of_birth: {
    default: new Date(),
    required: true
  },
  profile: {
    type: Number,
    required: true
  },
  home_phone: String,
  cel_phone: String,
  email: {
    type: String,
    required: true
  },
  admission_date: {
    default: new Date(),
    required: true
  },
  bond_type: {
    type: Number,
    required: true
  },
  resignation_date: {
    default: new Date()
  },
  civil_status: {
    type: Number,
    required: true
  },
  rg: {
    type: String,
    required: true
  },
  cpf: {
    type: String,
    required: true
  },
  supervisor: String,
  job_role: {
    type: Number,
    required: true
  },
  cost_hour: {
    type: Number,
    required: true
  },
  month_remuneration: {
    type: Number,
    required: true
  },
  bonus_hour: Number,
  remunerate_rest: String,
  bank: bankSchema,
  enterprise_data: EnterpriseSchema,
  user_pass: {
    type: String,
    required: true
  },
  active: Boolean
});

module.exports = ColaboratorsSchema;