const Schema = require('mongoose').Schema;

const BankSchema = new Schema({
  bank_number: {
    type: Number,
    required: true
  },
  agency: {
    type: Number,
    required: true
  },
  currency_account: {
    type: Number,
    required = true
  }
})

module.exports = BankSchema;