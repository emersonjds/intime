const Schema = require('mongoose').Schema;

const userSchema = new Schema({
    user: {
        type: String,
        required: true
    },
    passwd: {
        type: String,
        require: true,
    }
})

module.exports = userSchema