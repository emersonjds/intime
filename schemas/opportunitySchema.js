const Schema = require('mongoose').Schema;

const clientSchema = require('./clientSchema');
const revenueSchema = require('./revenueSchema')

const opportunitySchema = new Schema({
    opportunity_code: {
        type: Number,
        required: true
    },
    sale_date: {
        type: Date,
        default: new Date()
    },
    stage: {
        type: Number
    },
    description: {
        type: String,
        required: true
    },
    subsidiary: {
        type: String,
        required: true
    },
    client_corporate_name: {
        type: String,
        required: true
    },
    vd_vi: {
        type: String,
        required: true
    },
    project_code: Number,
    revenue_item: [revenueSchema],
    client: clientSchema
});


module.exports = opportunitySchema