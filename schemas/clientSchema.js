const Schema = require('mongoose').Schema;

const clientSchema = new Schema({
    erp_code: {
        type: Number,
        required: true
    },
    corporate_name: {
        type: String,
        require: true,
        uppercase: true
    },
    cnpj: {
        type: String,
        required: true
    },
    contact: {
        type: String,
        required: true
    },
    private_or_gov: {
        type: String,
        required: true
    },
    address: {
        street: String,
        cidy: String,
        state: String,
        cep: Number
    }
})

module.exports = clientSchema