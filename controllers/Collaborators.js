const mongoose = require(mongoose);
const collaboratorsSchema = require('../schemas/collaborators');

let model = mongoose.model('collaborators', collaboratorsSchema );

class CollaboratorsController {

  async list() {
    const result = await model.find({})
    return result;
  }

  async insert(collaborator) {
    const result = await model.create(collaborator)
    return !!result.id;
  }

  async edit(id, collaborator) {
    const result = await model.updateOne({_id: id}, {$set: collaborator})
    return !!result.nModified
  }

  async getById(id) {
    const result = await model.findOne({_id: id})
    return result;
  }

  async delete(id) {
    const result = await model.deleteOne({_id: id})
    return result
  }

}

module.exports = new CollaboratorsController