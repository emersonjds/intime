const mongoose = require('mongoose');
const opportunitySchema = require('../schemas/opportunitySchema')

let model = mongoose.model('opportunities', opportunitySchema)

class OpportunityController {

    async list() {
        const result = await model.find({})
        return result;
    }

    async insert(item) {
        const result = await model.create(item)
        return !!result.id;
    }

    async edit(id, item) {
        const result = await model.updateOne({ _id: id }, { $set: item })
        return !!result.nModified
    }

    async getById(id) {
        const result = await model.findOne({ _id: id })
        return result
    }

    async delete(id){
        const result = await model.deleteOne({_id: id})
        return result
    }

}

module.exports = new OpportunityController
