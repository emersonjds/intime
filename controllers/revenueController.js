const moongose = require('mongoose');
const revenueSchema = require('../schemas/revenueSchema');

let model = moongose.model('Revenue', revenueSchema);

class RevenueController {

    async list() {
        const result = await model.find({})
        return result;
    }

    async insert(revenue) {
        const result = await model.create(revenue)
        return !!result.id;
    }

    async edit(id, revenue) {
        const result = await model.updateOne({ _id: id }, { $set: item })
        return !!result.nModified
    }

    async getById(id) {
        const result = await model.findOne({ _id: id })
        return result
    }
}

module.exports = new RevenueController;