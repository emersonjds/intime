const mongoose = require('mongoose')
const userSchema = require('../schemas/userSchema')

let model = mongoose.model('users', userSchema)

class userController {

    async list() {
        const result = await model.find()
        return result;
    }

    async insert(item) {
        const result = await model.create(item)
        return !!result.id;
    }

    async edit(id, item) {
        const result = await model.updateOne({ _id: id },
            {
                $set: item
            })
        return !!result.nModified
    }

    async getById(id) {
        const result = await model.findOne({ _id: id })
        return result
    }

    async getByUser(user){
        const result = await model.findOne({user: user})
        return result
    }

    async validateSignIn(qry){

        const result = await model.findOne(qry)
        return result
    }

}

module.exports = new userController