const mongoose = require('mongoose')
const clientSchema = require('../schemas/clientSchema')

let model = mongoose.model('client', clientSchema)

class ClientController {

    async list() {
        const result = await model.find()
        return result;
    }

    async insert(client) {
        const result = await model.create(client)
        return !!result.id;
    }

    async edit(id, client) {
        const result = await model.updateOne({ _id: id },
            {
                $set: client
            })
        return !!result.nModified
    }

    async getById(id) {
        const result = await model.findOne({ _id: id })
        return result
    }

}

module.exports = new ClientController;