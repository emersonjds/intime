let createError = require('http-errors');
let express = require('express');
let path = require('path');
let cookieParser = require('cookie-parser');
let logger = require('morgan');
let bodyParser = require('body-parser')
let cors = require('cors');
let jwt = require('jsonwebtoken');

const { config } = require('dotenv');
if (process.env.NODE_ENV === 'production')
  config({ path: 'config/.env.prod' })
else
  config({ path: 'config/.env.dev' })

let opportunityRoutes = require('./routes/opportunityRoutes');
let userRoutes = require('./routes/userRoutes')
let clientRoutes = require('./routes/clientRoutes')

let app = express();
app.use(cors())
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Middleware para validar header Authorization
app.use(function(req, res, next) {
  let url = req.url
  let auth = req.headers.authorization
  if(url.indexOf('/signin') != -1 || url.indexOf('/signup') != -1 || url.indexOf('/api-docs') != -1 || url.indexOf('/healthz') != -1){
    return next()
  }
  if(!auth || !auth.startsWith('Bearer')){
    console.log('Header Authorization Not Found')
    return res.status(401).json({
      status: 'error',
      message: 'Token Não Localizado'
    })
  }
  else{
    auth = auth.split('Bearer').pop().trim()
    jwt.verify(auth, process.env.JWT_KEY, (err, data) => {
      if(err){
        return res.status(401).json({
          status: 'error',
          message: 'Invalid Token'
        })
      } 
      next()
    })
  }
})

// error handler
app.use(function(err, req, res, next) {
  
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send({
    message: err.message
  })
  
});

app.use('/opportunity', opportunityRoutes);
app.use('/user', userRoutes)
app.use('/client', clientRoutes)

app.get('/healthz', (req, res) => {
  res.send(`${new Date()}`);
});

// Configure Swagger

const expressSwagger = require('express-swagger-generator')(app);

let options = {
  swaggerDefinition: {
      info: {
          description: 'This is a sample server',
          title: 'Swagger',
          version: '1.0.0',
      },
      host: 'intime-api-server.herokuapp.com',
      basePath: '/',
      produces: [
          "application/json",
          "application/xml"
      ],
      schemes: ['https'],
      securityDefinitions: {
          JWT: {
              type: 'apiKey',
              in: 'header',
              name: 'Authorization',
              description: "",
          }
      }
  },
  basedir: __dirname, //app absolute path
  files: ['./routes/**/*.js'] //Path to the API handle folder
}

expressSwagger(options)

// Configure Mongoose

const Mongoose = require('mongoose');

let connected = false

function connectDb(){

  return new Promise((resolve, reject) => {
    
    if(connected){
      resolve()
      return
    }
  
    Mongoose.connect(process.env.MONGO_URL, { useNewUrlParser: true })

    const connection = Mongoose.connection
    connection.once('open', () => {
      console.log('db rodando')
      connected = true
      resolve()
    })

  })
}

connectDb()

module.exports = app;
