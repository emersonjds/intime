const swaggerUi = require('swagger-ui-express');
const swaggerJsDoc = require('swagger-jsdoc');

const options = {
  swaggerDefinition: {
    info: {
      swagger: "2.0",
      title: 'Test API',
      version: '1.0.0',
      description: 'Test Express Api with auto generate swagger doc'
    },
    basePath: '/',
  },
  apis: [
    '../routes/clientRoutes.js',
    '../routes/opportunityRoutes.js',
    '../routes/userRoutes.js'
  ]
}

const specs = swaggerJsDoc(options);

module.exports = (app) => {
  app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(specs));
}
