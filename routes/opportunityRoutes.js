let express = require('express');
let router = express.Router();
const opportunityController = require('../controllers/opportunityController')

/**
 * 
* @typedef Opportunity
* @property {number} opportunity_code
* @property {date} sale_date
* @property {number} stage
* @property {string} description
* @property {string} subsidiary
* @property {string} client_corporate_name
* @property {string} vd_vi
* @property {number} project_code
* @property {Array.<Revenue>} revenue_item
* @property {Client.model} client
*/
/**
 * @typedef Revenue
 * @property {string} product
 * @property {number} price
 * @property {string} description
 * @property {number} amount
 * @property {string} erp_product_code
 */
/**
* @typedef Client
* @property {number} erp_code
* @property {string} corporate_name
* @property {string} cnpj
* @property {string} contact
* @property {string} private_or_gov
* @property {Address.model} address
*/
/**
 * @typedef Address
 * @property {string} street
 * @property {string} city
 * @property {string} state
 * @property {number} cep
 */

/**
 * Recupera uma lista de oportunidades
 * @route GET /opportunity
 * @group Oportunidade - Ações com Oportunidades
 * @returns {Array.<Opportunity>} 200 - Lista de Oportunidades
 * @returns {string} 401 - Não Autorizado
 * @security JWT
 */
router.get('/', (req, res, next) => {
  opportunityController.list().then(list => {
    console.log(list)
    res.send(list)
  })
});
/**
 * Cria uma oportunidade
 * @route POST /opportunity/create
 * @param {Opportunity.model} Opportunity.body
 * @group Oportunidade
 * @returns {string} 200 - Save Sucessfull
 * @returns {string} 401 - Unauthorized
 * @security JWT
 */

router.post('/create', (req, res, next) => {
  if (!req.body)
    return new Error('Erro com os dados enviados')
    opportunityController.insert(req.body).then(() => {
      res.send('Dados gravados com sucesso')
  }).catch(e => {
    res.status(400).send({
      status: 'error',
      message: e.message
    })
  })
})

/**
 * Edita uma oportunidade
 * @route PATCH /opportunity/:id
 * @param {Opportunity.model} Opportunity.body
 * @group Oportunidade
 * @returns {string} 200 - Save Sucessfull
 * @returns {string} 401 - Unauthorized
 * @security JWT
 */
router.patch('/:id', (req, res, next) => {
  if(!req.body)
    return new Error('erro')
  let id = req.params.id
  let body = req.body
  opportunityController.edit(id, body).then((result)=> {
    let success = Number(result).toString()
    res.status(200).send(success)
  })
})

/**
 * Recupera uma oportunidade por Id
 * @route GET /opportunity/:id
 * @param {Opportunity.model} Opportunity.body
 * @group Oportunidade
 * @returns {string} 200 - Save Sucessfull
 * @returns {string} 401 - Unauthorized
 * @security JWT
 */
router.get('/:id', (req, res, next) => {
  if(!req.params)
    return new Error('erro')
  let id = req.params.id
  opportunityController.getById(id).then((result)=> {
    res.send(result)
  })
})

/**
 * Deleta uma oportunidade por Id
 * @route DELETE /opportunity/:id
 * @group Oportunidade
 * @returns {string} 200 - 0 - Falha ao deletar, 1 - Deletado com sucesso
 * @returns {string}  401 - Unauthorized
 * @security JWT
 */
router.delete('/:id', (req, res) => {
  if(!req.params)
    return new Error('erro')
  let id = req.params.id
  opportunityController.delete(id).then((result)=>{
    let success = result.n.toString()
    res.status(200).send(success)
  })
})

module.exports = router;
