let express = require('express');
let router = express.Router();
const clientController = require('../controllers/clientController')

/**
* @typedef Client
* @property {number} erp_code
* @property {string} corporate_name
* @property {string} cnpj
* @property {string} contact
* @property {string} private_or_gov
* @property {string} address
*/

/**
 * @typedef Address
 * @property {string} street
 * @property {string} city
 * @property {string} state
 * @property {number} cep
 */

/**
 * Recupera uma lista de clientes
 * @route GET /client
 * @group Cliente - Ações com Clientes
 * @returns {Client.model} 200 - Array de Clientes
 * @returns {Error}  401 - Não Autorizado
 * @security JWT
 */

router.get('/', (req, res, next) => {
  clientController.list().then(list => {
    console.log(list)
    res.send(list)
  })
});

/**
 * Recupera um cliente por Id
 * @route GET /client/{id}
 * @group Cliente
 * @returns {Client.model} 200 - OK
 * @returns {Error}  401 - Unauthorized
 * @returns {Error}  500 - Internal Server Error
 * @security JWT
 */

router.get('/get/:id', (req, res, next) => {
  if (!req.params)
    return new Error('erro')
  let id = req.params.id
  clientController.getById(id).then((result) => {
    res.send(result)
  })
})

/**
 * Cria um novo cliente
 * @route POST /client
 * @group Cliente
 * @returns {Client.model} 200 - Cliente criado com sucesso
 * @returns {Error}  401 - Unauthorized
 * @returns {Error}  500 - Internal Server Error
 * @security JWT
 */

router.post('/create', (req, res, next) => {
  if (!req.body)
    return new Error('Erro com os dados enviados')

  clientController.insert(req.body).then(() => {
    res.send('Dados gravados com sucesso')
  }).catch(e => {
    res.status(400).send({
      status: 'error',
      message: e.message
    })
  })
})
/**
 * @route PUT /client/{id}
 * @group Cliente
 * @returns {Client.model} 200 - Cliente editado com sucesso
 * @returns {Error}  401 - Unauthorized
 * @returns {Error}  500 - Internal Server Error
 * @security JWT
 */
/**
* @typedef Client
* @property {number} erp_code
* @property {string} corporate_name
* @property {string} cnpj
* @property {string} contact
* @property {string} private_or_gov
* @property {Address.model} address
*/
/**
 * @typedef Address
 * @property {string} street
 * @property {string} city
 * @property {string} state
 * @property {number} cep
 */
router.put('/edit', (req, res, next) => {
  if (!req.body)
    return new Error('erro')
  let id = req.body.id
  let item = req.body.item
  clientController.edit(id, item).then(() => {
    res.send('editado')
  })
})

module.exports = router;
