let express = require('express');
let router = express.Router();
const collaboratorsController = require("../controllers/Collaborators");
/**
 * 
* @typedef Collabotator
* @property {number} erp_code
* @property {date} name
* @property {number} gender
* @property {date} date_of_birth
* @property {string} profile
* @property {string} home_phone
* @property {string} cel_phone
* @property {string} email
* @property {date} admission_date
* @property {number} bond_type
* @property {date} resignation_date
* @property {number} civil_status
* @property {string} rg
* @property {string} cpf
* @property {string} supervisor
* @property {number} job_rule
* @property {number} cost_hour
* @property {number} month_remuneration
* @property {number} bonus_hour
* @property {string} remunerate_rest
* @property {Bank.model} bank
* @property {Enterprise.model} enterprise_data
* @property {string} user_passwd
* @property {boolean} active
*/
/**
 * @typedef Bank
 * @property {number} bank_number
 * @property {number} agency
 * @property {number} currency_account
 */
/**
* @typedef Enterprise
* @property {string} social_name
* @property {string} fantasy_name
* @property {string} cnpj
* @property {number} ie
* @property {string} avatar
*/
/**
 * Recupera uma lista de colaboradores
 * @route GET /collaborator
 * @group Colaboradores - Ações com Colaboradores
 * @returns {Array.<Collabotator>} 200 - Lista de Colaboradores
 * @returns {string} 401 - Não Autorizado
 * @security JWT
 */
router.get('/', (req, res, next) => {
  collaboratorsController.list().then(list => {
    console.log(list)
    res.send(list)
  })
})
/**
 * Cria uma oportunidade
 * @route POST /collaborator/create
 * @param {Collabotator.model} Collabotator.body
 * @group Colaboradores
 * @returns {string} 200 - Save Sucessfull
 * @returns {string} 401 - Unauthorized
 * @security JWT
 */

router.post('/create', (req, res, next) => {
  if (!req.body)
    return new Error('Erro com os dados enviados')
    collaboratorsController.insert(req.body).then(() => {
      res.send('Dados gravados com sucesso')
  }).catch(e => {
    res.status(400).send({
      status: 'error',
      message: e.message
    })
  })
})
/**
 * Edita um Colaborador
 * @route PATCH /collaborator/:id
 * @param {Collabotator.model} Collabotator.body
 * @group Colaboradores
 * @returns {string} 200 - Save Sucessfull
 * @returns {string} 401 - Unauthorized
 * @security JWT
 */
router.patch('/:id', (req, res, next) => {
  if(!req.body)
    return new Error('erro')
  let id = req.params.id
  let body = req.body
  collaboratorsController.edit(id, body).then((result)=> {
    let success = Number(result).toString()
    res.status(200).send(success)
  })
})
/**
 * Recupera um colaborador por Id
 * @route GET /collaborator/:id
 * @param {Collabotator.model} Collabotator.body
 * @group Colaboradores
 * @returns {string} 200 - Save Sucessfull
 * @returns {string} 401 - Unauthorized
 * @security JWT
 */
router.get('/:id', (req, res, next) => {
  if(!req.params)
    return new Error('erro')
  let id = req.params.id
  collaboratorsController.getById(id).then((result)=> {
    res.send(result)
  })
})

/**
 * Deleta uma colaborador por Id
 * @route DELETE /collaborator/:id
 * @group Colaboradores
 * @returns {string} 200 - 0 - Falha ao deletar, 1 - Deletado com sucesso
 * @returns {string}  401 - Unauthorized
 * @security JWT
 */
router.delete('/:id', (req, res) => {
  if(!req.params)
    return new Error('erro')
  let id = req.params.id
  collaboratorsController.delete(id).then((result)=>{
    let success = result.n.toString()
    res.status(200).send(success)
  })
})

module.exports = router;
