let express = require('express');
let router = express.Router();
let jwt = require('jsonwebtoken');
const userController = require('../controllers/userController')
/**
 * This function comment is parsed by doctrine
 * @route POST /user/signin
 * @group Login - Login no sistema
 * @param {UserRequest.model} User.body
 * @returns {string} 200 - ok
 * @returns {UserResponse.model} 200 - ok
 * @returns {Error} 401 - Unauthorized
 */
/**
* @typedef UserRequest
* @property {string} user
* @property {string} passwd
*/
/**
* @typedef UserResponse
* @property {string} user
* @property {string} token
*/
router.post('/signin', (req, res, next) => {
  let user = req.body.user
  let passwd = req.body.passwd

  let qry = {
    user: user,
    passwd: passwd
  }

  userController.validateSignIn(qry).then(result => {

    if (result === null) {
      res.status(401).send({
        status: 'error',
        message: 'Login ou Senha invalidos'
      })
    }

    userController.validateSignIn(qry).then(result => {
      if (result == null) {
        res.status(401).send({
          status: 'error',
          message: 'Login ou Senha invalidos'
        })
      }
      else {
        _result = result.toJSON()

        // Cria um token para este usuario
        let token = jwt.sign(_result, process.env.JWT_KEY, { expiresIn: '24h' });
        
        _result.token = token
        delete _result.passwd
        
        res.status(200).send({
          status: 'ok',
          message: _result
        })
      }
    })

  })
})

module.exports = router;